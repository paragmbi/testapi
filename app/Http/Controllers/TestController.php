<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Routing\ResponseFactory;
use DB; // USE Database Model
use App\Http\Model\Test;

class TestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function get($id)
    {
        /**
        if($id < 3){
            $output = [
                'code' => '1',
                'message' => 'success',
                'data' => [
                    'data' => [
                        'id' => $id
                    ],
                ],
            ];
        }
        else{
            $output = [
                'code' => '0',
                'message' => 'Id should be less than 2.',
                'data' => [],
            ];
        }
        return response()->json($output);
        /**/
        $Testdata = Test::where('id',$id)->first();
        if($Testdata){
            $output = [
                'code' => '1',
                'message' => 'success',
                'data' => [
                    'data' => [
                        'id' => $Testdata['id'],
                        'fname' => $Testdata['fname'],
                        'lname' => $Testdata['lname'],
                        'email' => $Testdata['email'],
                    ],
                ],
            ];
        }
        else{
             $output = [
                'code' => '0',
                'message' => 'Id should be less than 2.',
                'data' => [],
            ];
        }
        return response()->json($output);
    }
}
