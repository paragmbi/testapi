<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/**/
$router->get('/', function () use ($router) {
    return $router->app->version();
});
/**/
$router->group(['prefix' => 'api'], function () use ($router) {
	$router->get('/',[ 'as' => 'home', function(){
		return "web.php [LINE:".__LINE__."]";
	}]);

	/*
	$router->group(['middleware' => 'auth'], function() use ($router){
		$router->get('apiv1/', function(){
			//return __LINE__;
			redirect()->route('home');
		});
	});
	*/

	$router->group(['prefix' => 'apiv1'], function () use ($router) {
		$router->get('/', function () use ($router) {
		    //return redirect()->route('home');
		    return redirect()->route('apiv1.login');
		});

		$router->get('/get/{id:[0-9]+}', ['as' => 'apiv1.get', 'uses' => 'TestController@get']);

		$router->group(['middleware' => 'auth'], function () use ($router) {
		    $router->get('/login', [ 'as' => 'apiv1.login',function () use ($router) {
		        //return __LINE__;
		    }]);
	    });
	});
});